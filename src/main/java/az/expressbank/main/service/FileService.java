package az.expressbank.main.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;

@Service
public class FileService {
    public File sortFile(MultipartFile file) throws Exception {

        if (file == null || file.isEmpty()) {
            throw new RuntimeException("File cannot be empty or null");
        }

        //Reading from file
        String text = FileUtility.read(file);

        //Find numbers and get unsorted array
        Integer unSortedArray[] = findNumbers(text);

        //Sorting it by using bubble sort
        Integer sortedArray[] = bubbleSort(unSortedArray);

        //Write it into file named sorted.txt
        FileUtility.writeIntoFile("sorted.txt", sortedArray, true);

        // Taking file and return it
        File resultFile = new File(FileUtility.FILE_PATH + File.separator + "sorted.txt");

        return resultFile;
    }

    public Integer[] bubbleSort(Integer[] unSortedArray) {
        boolean isSwap;

        for (int i = 0; i < unSortedArray.length; i++) {
            isSwap = true;
            for (int j = 1; j < (unSortedArray.length - i); j++) {
                if (unSortedArray[j - 1] > unSortedArray[j]) {
                    int temp = unSortedArray[j - 1];
                    unSortedArray[j - 1] = unSortedArray[j];
                    unSortedArray[j] = temp;
                    isSwap = false;

                }
            }
            if (isSwap) {
                break;
            }
        }
        return unSortedArray;
    }


    public Integer[] findNumbers(String text) {
        int counter = 0;
        for (int i = 0; i < text.length(); i++) {
            if (Character.isDigit(text.charAt(i))) {
                counter++;
            }
        }

        Integer[] unsortedArray = new Integer[counter];
        int index = 0;
        for (int i = 0; i < text.length(); i++) {
            if (Character.isDigit(text.charAt(i))) {
                unsortedArray[index] = Character.getNumericValue(text.charAt(i));
                index++;
            }
        }


        return unsortedArray;
    }


}
