package az.expressbank.main.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public class FileUtility {
    public static final String FILE_PATH = "C:\\Users\\hp\\Desktop/Sorted_Files";
    public static File f = new File(FILE_PATH);


    public static void writeIntoFile(String fileName, Integer[] sortedArray, boolean a) throws Exception {
        if (!f.exists()) {
            f.mkdir();
        }
        FileWriter fw = new FileWriter(FILE_PATH + File.separator + fileName, a);
        try (BufferedWriter bw = new BufferedWriter(fw);) {
            for (Integer sortedElement : sortedArray) {
                bw.write(sortedElement.toString());
                bw.newLine();
            }

        }
        System.out.println("Successfully Done !");

    }

    public static String read(MultipartFile multipartFile) {
        StringBuilder text = new StringBuilder();
        try {
            InputStream is = multipartFile.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            while (br.ready()) {

                text.append(br.readLine());

            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return text.toString();
    }

    public static String read(String fileName) throws Exception {
        try (InputStream in = new FileInputStream(FILE_PATH + File.separator + fileName);
             InputStreamReader r = new InputStreamReader(in);
             BufferedReader reader = new BufferedReader(r);) {
            StringBuilder text = new StringBuilder();
            while (reader.ready()) {

                text.append(reader.readLine());

            }
            return text.toString();

        }

    }
}
