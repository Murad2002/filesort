package az.expressbank.main.controller;

import az.expressbank.main.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@RestController
@RequestMapping("/filesort")
@RequiredArgsConstructor
public class FileSortController {
    private final FileService fileService;

//    @PostMapping("/file")
//    public File sortfile(@RequestParam("file") MultipartFile file) throws Exception {
//        return fileService.sortFile(file);
//    }

    @PostMapping("/file")
    public FileSystemResource sortAndGetFileResult(@RequestParam("file") MultipartFile file) throws Exception {
        return new FileSystemResource(fileService.sortFile(file));
    }


}
